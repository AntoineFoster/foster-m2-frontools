/**
 * @author    Agence Dn'D <contact@dnd.fr>
 * @copyright 2018 Agence Dn'D
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      https://www.dnd.fr/
 */

'use strict';
module.exports = function() {
    var plugins = this.opts.plugins,
        configs = this.opts.configs,
        gulp = plugins.gulp,
        sprite = plugins.sprite,
        merge = plugins.merge,
        images = configs.files.sprite;

    var spriteData = gulp.src(images)
        .pipe(sprite({
            imgName: '../images/sprites.png',
            cssName: '_sprites.less'
        }));

    var imgStream = spriteData.img
        .pipe(gulp.dest(configs.path.images));

    var cssStream = spriteData.css
        .pipe(gulp.dest(configs.paths.spriteLess));

    return merge(imgStream, cssStream);
};
