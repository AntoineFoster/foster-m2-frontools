/**
 * @author    Agence Dn'D <contact@dnd.fr>
 * @copyright 2018 Agence Dn'D
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      https://www.dnd.fr/
 */

'use strict';
module.exports = function() {
    var plugins = this.opts.plugins,
        configs = this.opts.configs,
        gulp = plugins.gulp,
        lessHint = plugins.lessHint,
        reporter = plugins.reporter;

    return gulp.src(configs.files.lessToHint)
        .pipe(lessHint({
            configPath: './gulp-tasks/config/.lesshintrc'
        }))
        .pipe(lessHint.reporter())
        .pipe(lessHint.failOnError());
};
