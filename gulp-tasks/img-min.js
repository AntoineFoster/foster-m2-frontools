/**
 * @author    Agence Dn'D <contact@dnd.fr>
 * @copyright 2018 Agence Dn'D
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      https://www.dnd.fr/
 */

'use strict';
module.exports = function() {
    var plugins = this.opts.plugins,
        configs = this.opts.configs,
        images = configs.files.images,
        gulp = plugins.gulp,
        imagemin = plugins.imgMin;


    gulp.src(images)
        .pipe(imagemin({
            verbose: true
        }))
        .pipe(gulp.dest(configs.paths.images))
};
