/**
 * @author    Agence Dn'D <contact@dnd.fr>
 * @copyright 2018 Agence Dn'D
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      https://www.dnd.fr/
 */

'use strict';
module.exports = function() {
    var plugins = this.opts.plugins,
        configs = this.opts.configs,
        c = plugins.c,
        fs = plugins.fs,
        ncp = plugins.ncp,
        path = plugins.path,
        del = plugins.del;

    if (process.argv[3] == '--theme' && process.argv[5] == '--package' && process.argv[4] && process.argv[6]) {
        console.log(c.green('Good parameters'));

        var theme = process.argv[4];
        var themePackage = process.argv[6];
        console.log('Theme => ' + theme);
        console.log('Package => ' + themePackage);

        var themePath = '../../../app/design/frontend/' + theme + '/' + themePackage;

        if (fs.existsSync(themePath)) {
            console.log(c.green('Directory exist'));

            var srcPath = configs.paths.gulp;

            ncp(srcPath, themePath, function( err) {
                if (err) {
                    return console.error(err);
                }
                del([
                    themePath + '/gulp-tasks/setup.js'
                ], {force: true});
                console.log(c.green('Copying files complete.'));
            });

        } else {
            console.log(c.red('app/design/frontend/'+ theme +'/' + themePackage + '  Directory does not exist'));
        }
    } else {
        console.log(c.red(`Please specify your theme and package => ${c.yellow('gulp setup --theme Codmag --package default')}`));
    }
};
