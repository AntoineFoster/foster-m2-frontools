/**
 * @author    Agence Dn'D <contact@dnd.fr>
 * @copyright 2018 Agence Dn'D
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      https://www.dnd.fr/
 */

'use strict';
module.exports = function() {
    var plugins = this.opts.plugins,
        configs = this.opts.configs,
        paths = configs.paths,
        files = configs.files,
        gulp = plugins.gulp,
        iconfont = plugins.iconfont,
        iconfontCss = plugins.iconfontCss,
        svgicons = files.svgicons;

    console.log('Ok');
};
