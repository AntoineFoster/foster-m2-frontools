/**
 * @author    Agence Dn'D <contact@dnd.fr>
 * @copyright 2018 Agence Dn'D
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      https://www.dnd.fr/
 */

'use strict';
module.exports = function() {
    var plugins = this.opts.plugins,
        configs = this.opts.configs,
        paths = configs.paths,
        files = configs.files,
        gulp = plugins.gulp,
        iconfont = plugins.iconfont,
        iconfontCss = plugins.iconfontCss,
        svgicons = files.svgicons;

    return gulp.src(svgicons)
        .pipe(iconfontCss({
            fontName: configs.iconfont.name,
            path: './web/css/source/dnd-lib/_icons.less',
            targetPath: '../../css/source/iconfont/_icons.less',
            fontPath: 'fonts/'+configs.iconfont.name+'/',
            cssClass: configs.iconfont.cssClass
        }))
        .pipe(iconfont({
            fontName: configs.iconfont.name,
            fontHeight: 1001,
            formats: configs.iconfont.formats,
            normalize: true
        }))
        .pipe(gulp.dest(paths.font+configs.iconfont.name));
};
