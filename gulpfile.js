/**
 * @author    Agence Dn'D <contact@dnd.fr>
 * @copyright 2018 Agence Dn'D
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @link      https://www.dnd.fr/
 */

'use strict';

/* ==========================================
 *
 *  Gulpfile.js for Magento 2 theme
 *  Author: Agence Dn'D
 *
 *    Table of content :
 *
 *    - PLUGINS
 *      > gulp
 *      > gulp-lesshint
 *      > gulp-reporter
 *      > gulp-iconfont
 *      > gulp-iconfont-css
 *      > gulp.spritesmith
 *      > gulp-imagemin
 *      > merge-stream
 *
 *    - CONFIGS
 *      > files
 *          - lessToHint
 *          - svgicons
 *          - images
 *      > iconfont
 *          - name
 *          - formats
 *          - cssClass
 *      > paths
 *          - font
 *          - images
 *
 *    - TASK LOADER
 * ========================================== */

var plugins = {};
var configs = {};
var taskLoader = require('gulp-task-loader');

plugins.gulp = require('gulp');
plugins.lessHint = require('gulp-lesshint');
plugins.reporter = require('gulp-reporter');
plugins.iconfont = require('gulp-iconfont');
plugins.iconfontCss = require('gulp-iconfont-css');
plugins.sprite = require('gulp.spritesmith');
plugins.imgMin = require('gulp-imagemin');
plugins.merge = require('merge-stream');
plugins.c = require('ansi-colors');
plugins.fs = require('fs');
plugins.ncp = require('ncp');
plugins.path = require('path');
plugins.del = require('del');

configs.files = {
    lessToHint: [
        './web/css/source/*.less',
        './*/web/css/*.less',
        './*/web/css/source/*.less',
        './*/web/css/source/module/*.less',
        '!./web/css/source/_email-base.less',
        '!./web/css/source/_extends.less',
        '!./web/css/source/_forms.less',
        '!./web/css/source/_icons.less'
    ],
    svgicons: ['./web/images/svg-icons/*.svg'],
    images: [
        './web/images/*.png',
        './web/images/*.jpg',
        './web/images/*.gif',
        './web/images/*.jpeg',
        './web/images/*/*.png',
        './web/images/*/*.jpg',
        './web/images/*/*.gif',
        './web/images/*/*.jpeg'
    ],
    sprite: [
        './web/images/sprites/*.png'
    ]
};

configs.iconfont = {
    name: 'codmag-iconfont',
    formats: [
        'ttf',
        'eot',
        'woff',
        'woff2',
        'svg'
    ],
    cssClass: 'atomag-icon'
};

configs.paths = {
    font: './web/fonts/',
    images: './web/images',
    spriteLess: './web/css/source/sprites/',
    gulp: __dirname
};

taskLoader({
    dir: 'gulp-tasks',
    plugins: plugins,
    configs: configs
});
